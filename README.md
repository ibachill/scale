
----------------
 run validation
----------------

 produces data-MC distributions to validate 

 ./cmsTreeRun DileptonAnalyzer.C Validation 0.0 TUNEP validation_folder/ |& tee out.txt

-------------
 run scale
-------------

  injecting one bias for test, for exaple 0.0:

  ./cmsTreeRun DileptonAnalyzer.C Scale 0.0 TUNEP scale_folder/

  to run over all the bias:

  nohup python send_jobs.py ./SuperLaunchGeneralized_TuneP.sh &

  often it does not run over one or two values of bias, you should run afterwards 

-------------
 chi 2 test
-------------


  we can cut on pT on: GeneralizedEndpointSimplified.C // PT CUT 

  change also input_folder and output_folder

  root -l -q -b GeneralizedEndpointSimplified.C+

  it produces the plots of the fits and a txt for the map


-------------
 plot map
-------------

  root -l -q -b Wprime_ScaleMap.C+



